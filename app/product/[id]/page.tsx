// // import AddToCart from "@/app/Common/addToCart";
// // import StarRating from "@/app/Common/starRating";
// // import { fetchProducts } from "";
// import { Metadata, ResolvingMetadata } from "next";
// import Image from "next/image";
// const API_URL = 'https://fakestoreapi.com/';

// type Product = {
//   id: number;
//   title: string;
//   price: number;
//   image: string;
//   description: string;
//   rate?: number;
//   rating?: Rating;
// };

// type Rating = {
//   rate: number;
//   count: number;
// };

// type Props = {
//   params: {
//     id: string;
//     category: string;
//   };
// };

// export async function generateMetadata(
//   { params }: Props,
//   parent: ResolvingMetadata
// ): Promise<Metadata> {
// //   const products = await fetchProducts();
//   const products = await fetch(`${API_URL}product`);
//   const product = products.find((p: Product) => p.id === Number(params.id));

//   return (
//     product && {
//       title: product.title,
//       description: product.description,
//     }
//   );
// }

// const Page = async ({ params }: Props) => {
//   const products = await  await fetch(`${API_URL}product`);
//   const product = products.find((p: Product) => p.id === Number(params.id));

//   return (

//   );
// };

// export default Page;

import { Metadata, ResolvingMetadata } from "next";
import Image from "next/image";

const API_URL = "https://fakestoreapi.com/";

type Product = {
	id: number;
	title: string;
	price: number;
	image: string;
	description: string;
	rate?: number;
	rating?: Rating;
};

type Rating = {
	rate: number;
	count: number;
};

type Props = {
	params: {
		id: string;
		category: string;
	};
};

export async function generateMetadata(
	{ params }: Props,
	parent: ResolvingMetadata
): Promise<Metadata> {
	const products = await fetchProducts();
	const product = products.find((p: Product) => p.id === Number(params.id));

	return product
		? {
				title: product.title,
				description: product.description,
		  }
		: {};
}

async function fetchProducts(): Promise<Product[]> {
	const response = await fetch(`${API_URL}products`);
	const data = await response.json();
	return data;
}

const Page = async ({ params }: Props) => {
	const products = await fetchProducts();
	const product = products.find((p: Product) => p.id === Number(params.id));

	return (
		<>
			<div className="relative flex justify-center items-start top-[100px] w-full gap-10 h-[100vh]">
				{product ? (
					<>
						<div className="flex justify-center items-center bg-gray-200 rounded-xl">
							<div className="flex justify-center items-center w-full">
								<Image
									src={product.image}
									height={500}
									width={500}
									alt="image"
									className="object-contain h-[350px]"
								/>
							</div>
						</div>
						<div className="flex items-end justify-between w-[60%]">
							<div className="flex items-center justify-center gap-10 h-[400px]">
								<div className="text-black flex flex-col gap-3 justify-start items-start text-start w-[90%]">
									<h2 className="text-3xl font-bold">
										{product.title}
									</h2>
									{/* <div className="text-sm font-semibold flex gap-2">
                    Review <StarRating rating={product.rating?.rate || 0} />
                  </div> */}
									<p className="flex justify-start items-start text-start text-2xl w-full font-semibold gap-2">
										<span className="text-slate-500">
											${product.price}
										</span>
									</p>
									<h3 className="font-semibold text-stone-700">
										About Product:
									</h3>
									<p className="capitalize text-stone-700 font-bold font-serif leading-6 text-sm">
										{product.description}
									</p>
									<hr className="w-full h-1 bg-gray-200"></hr>
									<div>
										{/* <AddToCart
                      className=""
                      image={product.image}
                      price={product.price}
                      name={product.title}
                      id={product.id}
                    /> */}
									</div>
								</div>
							</div>
						</div>
					</>
				) : (
					<div>No Products Found</div>
				)}
			</div>
		</>
	);
};

export default Page;
