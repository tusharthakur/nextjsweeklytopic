 export default function AboutUs(){
    return (
        <div className="p-16 italic border-2 border-gray-500 hover:border-sky-600"> 
           Our team is a diverse tapestry of talent, each thread weaving its unique story into the
           fabric of our success. From the seasoned veterans who bring a wealth of experience to the
           fresh minds bubbling with creativity, we embrace the power of collaboration. It's not just
           about what we do; it's about how we do it, fostering an environment where every voice is heard,
           and every idea has the potential to redefine the status quo. 
        </div>
    )
}