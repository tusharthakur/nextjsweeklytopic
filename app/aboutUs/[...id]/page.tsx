import React from 'react';
type PostProps = {
    params: {id: string };
}

const  Post: React.FC<PostProps> = ({params})=>{
  return (
        <div className='h-[100vh]'>
        <h1 className='text-red-500'>text: {params.id}</h1>
        </div>
      );
}

export default Post;