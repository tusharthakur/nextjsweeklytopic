// "use client";
// import Link from "next/link";
// import React, { useEffect } from "react";
// import { useRouter } from "next/navigation";
// import axios from "axios";
// import Image from "next/image";
// // import Background from "../../../public/img1.jpeg";
// import toast from "react-hot-toast";

// export default function signup() {
// 	const router = useRouter();
// 	const [user, setUser] = React.useState({
// 		email: "",
// 		password: "",
// 		username: "",
// 	});
// 	const [buttonDisabled, setButtonDisabled] = React.useState(false);
// 	const [loading, setLoading] = React.useState(false);

// 	const onSignup = async () => {
// 		try {
// 			setLoading(true);
// 			let response = await axios.post("", user);
// 			console.log("Sign-up success", response.data);
// 			router.push("/login");
// 		} catch (error: any) {
// 			setLoading(false)
// 			console.log("Sign-up failed.", error.message);
// 			toast.error(error.message);
// 		}
// 	};


// 	useEffect(() => {
// 		if (
// 			user.username.length > 0 &&
// 			user.email.length > 0 &&
// 			user.password.length > 0
// 		) {
// 			setButtonDisabled(false);
// 		} else {
// 			setButtonDisabled(true);
// 		}
// 	}, [user]);

// 	return (
// 		<div>
// 			<div className="flex items-center justify-center min-h-screen py-2 bg-violet-300">
// 				<div className="w-1/3 h-1/2 bg-white text-black py-6 px-8">
// 					<div className="py-6 font-medium text-2xl">Signup</div>
// 					<div>
// 						<div className="flex flex-cols pt-6 border-b-2">
// 							<div className="p-2 text-violet-700">
// 								<svg
// 									xmlns="http://www.w3.org/2000/svg"
// 									width="16"
// 									height="16"
// 									fill="currentColor"
// 									className="bi bi-person-circle"
// 									viewBox="0 0 16 16">
// 									<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
// 									<path
// 										fill-rule="evenodd"
// 										d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
// 									/>
// 								</svg>
// 							</div>
// 							<input
// 								id="username"
// 								type="text"
// 								value={user.username}
// 								onChange={(e) =>
// 									setUser({
// 										...user,
// 										username: e.target.value,
// 									})
// 								}
// 								placeholder="Enter your username."
// 							/>
// 						</div>
// 						<div className="flex flex-cols pt-6 border-b-2">
// 							<div className="p-2 text-violet-700">
// 								<svg
// 									xmlns="http://www.w3.org/2000/svg"
// 									width="16"
// 									height="16"
// 									fill="currentColor"
// 									className="bi bi-envelope-fill"
// 									viewBox="0 0 16 16">
// 									<path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z" />
// 								</svg>
// 							</div>
// 							<input
// 								id="email"
// 								type="text"
// 								value={user.email}
// 								onChange={(e) =>
// 									setUser({
// 										...user,
// 										email: e.target.value,
// 									})
// 								}
// 								placeholder="Enter your email."
// 							/>
// 						</div>
// 						<div className="flex flex-cols pt-6 border-b-2">
// 							<div className="p-2 text-violet-700">
// 								<svg
// 									xmlns="http://www.w3.org/2000/svg"
// 									width="16"
// 									height="16"
// 									fill="currentColor"
// 									className="bi bi-shield-lock-fill"
// 									viewBox="0 0 16 16">
// 									<path
// 										fill-rule="evenodd"
// 										d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm0 5a1.5 1.5 0 0 1 .5 2.915l.385 1.99a.5.5 0 0 1-.491.595h-.788a.5.5 0 0 1-.49-.595l.384-1.99A1.5 1.5 0 0 1 8 5z"
// 									/>
// 								</svg>
// 							</div>
// 							<input
// 								id="password"
// 								type="text"
// 								value={user.password}
// 								onChange={(e) =>
// 									setUser({
// 										...user,
// 										password: e.target.value,
// 									})
// 								}
// 								placeholder="Enter your password."
// 							/>
// 						</div>
// 					</div>
// 					<div className="pt-4 pb-2">
// 						<button
// 							onClick={onSignup}
// 							className="w-full py-2 rounded-md font-semibold text-white bg-violet-500 hover:bg-violet-700">
// 							{buttonDisabled
// 								? "Add Values"
// 								: loading
// 								? "Processing"
// 								: "Sign-up"}
// 						</button>
// 					</div>
// 					<div className="flex items-center">
// 						<div className="py-2 text-sm">Have an account?</div>
// 						<div onClick={()=>router.push("/login") } className="pl-1 py-2 text-sm text-violet-600 cursor-pointer">
// 							Log in now
// 						</div>
// 					</div>
// 				</div>
// 			</div>
// 		</div>
// 	);
// }
