import Title from "./title";
type headerProps = {
    title: string,
}

export default function Header({title}: headerProps) {
	return (
		<main>
			<div className="bg-[#3cb6d3]">
				<div className="p-3 flex justify-between">
					<div className="p-2 flex justify-between gap-2">
						<div>
							<i className="fa-solid fa-bars pt-1 text-lg"></i>
						</div>
						<div className = "flex">
						<div className= "pt-0.5">
							<i className="fa-solid fa-location-pin text-2xl"></i>
						</div>
						<div className="font-semibold text-xl">{title}</div>
						</div>
					</div>
					<div className="md:w-56 flex justify-between gap-2">
						<div className="w-28 h-10 py-1 px-4 flex gap-2 justify-center items-center bg-white border border-transparent rounded-full text-md text-cyan-500">
							{/* <div>
                                {icon && <Icon  className="h-4 w-4" icon={icon} />}
							</div> */}
							<div className="font-semibold block"><Title title="Sign-In"/></div>
						</div>
						<div className="max-sm:hidden py-1 px-4 w-auto h-10 flex items-center bg-white border border-transparent rounded-full text-md text-cyan-500 lg:hidden">
							<div className="font-semibold block">
								<Title className="whitespace-nowrap" title="About-Us" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	);
}