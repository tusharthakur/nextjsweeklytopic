import React from "react";

type titleProps = {
	className?: string;
	title: string;
};

const Title = ({ className, title }: titleProps) => {
	return (
		<div className={`flex justify-center text-center ${className}`}>
			{title}
		</div>
	);
};

export default Title;