"use client";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import axios from "axios";
// import Image from "next/image";
// import Background from "../../../public/img2.jpeg";
import toast from "react-hot-toast";

export default function login() {
	const router = useRouter(); //router is used for redirecting to new page.

	// created useState for user.
	const [user, setUser] = React.useState({
		username: "",
		password: "",
	});

	const [visiblePassword, setVisiblePassword] = useState<boolean>(false);

	//useState for button disabled.
	const [buttonDisabled, setButtonDisabled] = React.useState(false);
	//useState for loading.
	const [loading, setLoading] = React.useState(false);

	const onlogin = async () => {
		try {
			setLoading(true);
			// let response = await axios.post("https://fakestoreapi.com/auth/login", user);
			// console.log("Login successful", response.data);
			let response = await fetch("https://fakestoreapi.com/auth/login", {
				method: "POST",
				body: JSON.stringify({
					username: "mor_2314",
					password: "83r5^_",
				}),
			})
			toast.success("Login Successful.");
			router.push("/");
		} catch (error: any) {
			console.log("Login failed.", error.message);
			toast.error(error.message);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		if (user.username.length > 0 && user.password.length > 0) {
			setButtonDisabled(false);
		} else {
			setButtonDisabled(true);
			console.log(user.username, user.password);
		}
	}, [user]);

	return (
		<div>
			<div className="flex items-center justify-center min-h-screen py-2 bg-stone-300">
				<div className="w-1/3 h-1/2 bg-white text-black py-6 px-8 rounded-l-lg">
					<div className="py-6 font-medium text-2xl">Login</div>
					<div>
						<div className="flex flex-cols pt-6 border-b-2">
							<div className="p-2 text-zinc-300">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									className="bi bi-envelope-fill"
									viewBox="0 0 16 16">
									<path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z" />
								</svg>
							</div>
							<input
								id="username"
								type="text"
								value={user.username}
								onChange={(e) =>
									setUser({
										...user,
										username: e.target.value,
									})
								}
								placeholder="Enter your username."
							/>
						</div>
						<div className="pt-6 border-b-2">
							<div className="flex justify-between">
								<div className="flex justify-center items-center p-2  text-zinc-300">
									<svg
										xmlns="http://www.w3.org/2000/svg"
										width="16"
										height="16"
										fill="currentColor"
										className="bi bi-shield-lock-fill"
										viewBox="0 0 16 16">
										<path
											fill-rule="evenodd"
											d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm0 5a1.5 1.5 0 0 1 .5 2.915l.385 1.99a.5.5 0 0 1-.491.595h-.788a.5.5 0 0 1-.49-.595l.384-1.99A1.5 1.5 0 0 1 8 5z"
										/>
									</svg>
									<input
										id="password"
										type={
											visiblePassword
												? "text"
												: "password"
										}
										value={user.password}
										onChange={(e) =>
											setUser({
												...user,
												password: e.target.value,
											})
										}
										placeholder="Enter your password."
										className="px-2"
									/>
								</div>
								<div
									className="flex items-center"
									onClick={() =>
										setVisiblePassword(!visiblePassword)
									}>
									{visiblePassword ? (
										<svg
											xmlns="http://www.w3.org/2000/svg"
											width="16"
											height="16"
											fill="currentColor"
											className="bi bi-eye-fill"
											viewBox="0 0 16 16">
											<path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
											<path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
										</svg>
									) : (
										<svg
											xmlns="http://www.w3.org/2000/svg"
											width="16"
											height="16"
											fill="currentColor"
											className="bi bi-eye-slash-fill"
											viewBox="0 0 16 16">
											<path d="m10.79 12.912-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z" />
											<path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829zm3.171 6-12-12 .708-.708 12 12-.708.708z" />
										</svg>
									)}
								</div>
							</div>
						</div>
					</div>
					<div
						// onClick={() => router.push("/resetPassword")}
						className="py-2 text-sm text-sky-600 cursor-pointer">
						Forgot password?
					</div>
					<div className="py-2">
						<button
							onClick={onlogin}
							className="w-full py-2 rounded-md font-semibold text-white bg-zinc-300 hover:bg-sky-600">
							{buttonDisabled
								? "Add Values"
								: loading
								? "Processing"
								: "Login"}
						</button>
					</div>
					<div className="flex items-center ">
						<div className="py-2 text-sm">
							Don't have an account ?
						</div>
						<div
							// onClick={() => router.push("/signup")}
							className="py-2 pl-1 text-sm text-sky-600 cursor-pointer">
							Sign-up
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
